use std::{array, f64::consts, fmt};

use arrayvec::ArrayVec;
const OPERATIONS: u8 = 19;
const OP_COUNT: usize = 8;
fn target(x: f64) -> f64 {
    x.clamp(0.0, 1.0)
}
#[derive(Copy, Clone, Debug)]
enum UnaryFunction {
    Sqrt,
    Sin,
    Cos,
    Asin,
    Ln,
}
#[derive(Copy, Clone, Debug)]
enum BinaryFunction {
    Add,
    Sub,
    Mul,
    Div,
    Pow,
}
#[derive(Clone, Debug)]
enum Program {
    X,
    Constant(f64),
    UnaryFunction(UnaryFunction, Box<Program>),
    BinaryFunction(BinaryFunction, Box<Program>, Box<Program>),
}
impl Program {
    fn evaluate(&self, value: f64) -> f64 {
        match self {
            Self::X => value,
            Self::Constant(x) => *x,
            Self::UnaryFunction(f, p) => {
                let a = p.evaluate(value);
                match f {
                    UnaryFunction::Sqrt => a.sqrt(),
                    UnaryFunction::Sin => a.sin(),
                    UnaryFunction::Cos => a.cos(),
                    UnaryFunction::Asin => a.asin(),
                    UnaryFunction::Ln => a.ln(),
                }
            }
            Self::BinaryFunction(f, p, q) => {
                let a = p.evaluate(value);
                let b = q.evaluate(value);
                match f {
                    BinaryFunction::Add => a + b,
                    BinaryFunction::Sub => a - b,
                    BinaryFunction::Mul => a * b,
                    BinaryFunction::Div => a / b,
                    BinaryFunction::Pow => a.powf(b),
                }
            }
        }
    }
    fn from_rpn(operations: [u8; OP_COUNT]) -> Option<Self> {
        let mut parts: ArrayVec<Self, OP_COUNT> = ArrayVec::new();
        for op in operations {
            let new = match op {
                0 => Self::X,
                1 => Self::Constant(1.0),
                2 => Self::Constant(2.0),
                3 => Self::Constant(3.0),
                4 => Self::Constant(4.0),
                5 => Self::Constant(5.0),
                6 => Self::Constant(6.0),
                7 => Self::Constant(consts::E),
                8 => Self::Constant(consts::PI),
                9 => Self::UnaryFunction(UnaryFunction::Sqrt, Box::new(parts.pop()?)),
                10 => Self::UnaryFunction(UnaryFunction::Sin, Box::new(parts.pop()?)),
                11 => Self::UnaryFunction(UnaryFunction::Cos, Box::new(parts.pop()?)),
                12 => Self::UnaryFunction(UnaryFunction::Asin, Box::new(parts.pop()?)),
                13 => Self::UnaryFunction(UnaryFunction::Ln, Box::new(parts.pop()?)),
                14 => {
                    let last = parts.pop()?;
                    Self::BinaryFunction(
                        BinaryFunction::Add,
                        Box::new(parts.pop()?),
                        Box::new(last),
                    )
                }
                15 => {
                    let last = parts.pop()?;
                    Self::BinaryFunction(
                        BinaryFunction::Sub,
                        Box::new(parts.pop()?),
                        Box::new(last),
                    )
                }
                16 => {
                    let last = parts.pop()?;
                    Self::BinaryFunction(
                        BinaryFunction::Mul,
                        Box::new(parts.pop()?),
                        Box::new(last),
                    )
                }
                17 => {
                    let last = parts.pop()?;
                    Self::BinaryFunction(
                        BinaryFunction::Div,
                        Box::new(parts.pop()?),
                        Box::new(last),
                    )
                }
                _ => {
                    let last = parts.pop()?;
                    Self::BinaryFunction(
                        BinaryFunction::Pow,
                        Box::new(parts.pop()?),
                        Box::new(last),
                    )
                }
            };
            parts.push(new);
        }
        parts.pop()
    }
    fn optimise(&self) -> Self {
        match self {
            Self::X | Self::Constant(_) => self.clone(),
            Self::UnaryFunction(f, p) => {
                let p = p.optimise();
                if let Self::Constant(c) = p {
                    Self::Constant(
                        Self::UnaryFunction(*f, Box::new(Self::Constant(c))).evaluate(0.0),
                    )
                } else {
                    Self::UnaryFunction(*f, Box::new(p))
                }
            }
            Self::BinaryFunction(f, p, q) => {
                let p = p.optimise();
                let q = q.optimise();
                if let (Self::Constant(c), Self::Constant(d)) = (&p, &q) {
                    Self::Constant(
                        Self::BinaryFunction(
                            *f,
                            Box::new(Self::Constant(*c)),
                            Box::new(Self::Constant(*d)),
                        )
                        .evaluate(0.0),
                    )
                } else {
                    Self::BinaryFunction(*f, Box::new(p), Box::new(q))
                }
            }
        }
    }
}
fn bracket(p: &Program) -> String {
    match p {
        Program::BinaryFunction(_, _, _) => format!("({p})"),
        _ => p.to_string(),
    }
}
impl fmt::Display for UnaryFunction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Sin => "sin",
                Self::Cos => "cos",
                Self::Asin => "arcsin",
                Self::Ln => "ln",
                Self::Sqrt => "√",
            },
        )
    }
}
impl fmt::Display for BinaryFunction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Add => "+",
                Self::Sub => "−",
                Self::Mul => "⋅",
                Self::Div => "∕",
                Self::Pow => "^",
            }
        )
    }
}
impl fmt::Display for Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::X => write!(f, "x"),
            Self::Constant(c) => {
                if *c == consts::E {
                    write!(f, "e")
                } else if *c == consts::PI {
                    write!(f, "π")
                } else {
                    write!(f, "{c}")
                }
            }
            Self::UnaryFunction(g, p) => {
                write!(f, "{g}({p})")
            }
            Self::BinaryFunction(g, p, q) => {
                write!(f, "{} {g} {}", bracket(p), bracket(q))
            }
        }
    }
}
#[derive(Clone, Debug)]
struct Operations {
    operations: [u8; OP_COUNT],
}
impl Operations {
    fn new() -> Self {
        Self {
            operations: [0; OP_COUNT],
        }
    }
}
impl Iterator for Operations {
    type Item = [u8; OP_COUNT];
    fn next(&mut self) -> Option<Self::Item> {
        for j in (0..OP_COUNT).rev() {
            self.operations[j] += 1;
            if self.operations[j] == OPERATIONS {
                self.operations[j] = 0;
                if j == 0 {
                    return None;
                }
            } else {
                break;
            }
        }
        Some(self.operations)
    }
}
const TARGET_POINT_COUNT: usize = 100;
fn target_point(t: f64) -> f64 {
    ((t - 0.5) * 10.0).powi(3)
}
fn loss(target: f64, actual: f64) -> f64 {
    (actual - target).powi(2)
}
fn main() {
    let mut best_score = f64::INFINITY;
    let targets: [(f64, f64); TARGET_POINT_COUNT] =
        array::from_fn(|i| target_point(i as f64 / (TARGET_POINT_COUNT - 1) as f64))
            .map(|x| (x, target(x)));
    for program in Operations::new().filter_map(Program::from_rpn) {
        let optimised = program.optimise();
        let mut score = 0.0;
        for (val, target_value) in &targets {
            let actual = optimised.evaluate(*val);
            score += loss(*target_value, actual).powi(2);
            if score >= best_score {
                break;
            }
        }

        if score < best_score {
            best_score = score;
            println!("{program} {best_score:?}");
        }
    }
}
