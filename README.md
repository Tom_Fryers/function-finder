# Function Finder

This program can find functions that closely approximate a given function.

The target function is defined in `main.rs`:

```rust
fn target(x: f64) -> f64 {
    x.clamp(0.0, 1.0)
}
```

Modify this definition to find approximations to a different function.

The maximum complexity of the resulting function is given by `OP_COUNT` (which
is 8 by default).

By default, the optimiser minimises the residual sum of squares over points
in [−125, 125], clustered around zero.

`targets` can be changed to adjust this.
